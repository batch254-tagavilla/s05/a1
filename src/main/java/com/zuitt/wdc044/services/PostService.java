package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    // create a post
    void createPost(String stringToken, Post post);

    // getting all post
    Iterable<Post> getPosts();

    // Edit a users post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete a users post
    ResponseEntity deletePost(Long id, String stringToken);

    // Retrieve all post of a specific user
    Iterable<Post> getUserPosts(String stringToken);

}
