package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // create a post function
    public void createPost(String stringToken, Post post) {
        // findByUsername to retrieve the user
        // criteria for finding the user is from the jwtToke method getUsernameFromToken.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        // the title and content will come from the reqBody which is passed through the post identifier
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        // author retrieve from the token
        newPost.setUser(author);

        //  the actual saving of post in our table
        postRepository.save(newPost);
    }

    // getting all posts
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    // edit a post
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        // we will retrieve the post we want to update using the "id" provided
        Post postForUpdating = postRepository.findById(id).get();

        // because relationship is established within the User and Post models, we are able to retrieve the username of the post owner
        String postAuthor = postForUpdating.getUser().getUsername();

        // we will retrieve the username of the currently logged-in user from his token
        // Request Headers Authorization
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        // This will check if the logged-in user is the owner of the post
        if (authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        }
        // if the user from the token didn't match with the user from the post, we will warn them
        else {
            return new ResponseEntity<>("You are not authorized to edit this post!", HttpStatus.UNAUTHORIZED);
        }
    }

    // delete post
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeletion = postRepository.findById(id).get();
        String postAuthor = postForDeletion.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("You are not authorized to delete this post!", HttpStatus.UNAUTHORIZED);
        }
    }

    // Retrieve all post of a specific user
    public Iterable<Post> getUserPosts(String stringToken) {
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User user = userRepository.findByUsername(authenticatedUser);

        return postRepository.findByUser(user);
    }
}
